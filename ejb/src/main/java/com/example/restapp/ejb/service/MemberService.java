package com.example.restapp.ejb.service;

import com.example.restapp.ejb.model.Member;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;

/**
 * Created by gabriel on 6/21/17.
 */

@Stateless
public class MemberService {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private Event<Member> memberEventSrc;

    public void register(Member member) throws Exception {
        log.info("Registering " + member.getName());
        em.persist(member);
        memberEventSrc.fire(member);
    }

    public void update(long id, Member currMember) throws Exception {
        log.info("Updating " + currMember.getName());

        Member m = em.find(Member.class, id);

        m.setName(currMember.getName());
        m.setEmail(currMember.getEmail());
        m.setPhoneNumber(currMember.getPhoneNumber());
        em.merge(m);

        memberEventSrc.fire(m);
    }

    public void delete(long id) throws Exception {
        Member m = em.find(Member.class, id);
        log.info("Deleting " + m.getName());
        em.remove(m);
        memberEventSrc.fire(m);
    }
}
