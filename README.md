# Proyecto base para RESTful Web Service utilizando Java EE 7 (JAX-RS + EJB3) #


## Creación del proyecto desde cero: ##

### Crear la raíz del proyecto ###
	
```
#!bash

$ mvn archetype:generate -DarchetypeGroupId=org.codehaus.mojo.archetypes \
 -DarchetypeArtifactId=pom-root \
 -DarchetypeVersion=RELEASE \
 -DgroupId=com.example.restapp \
 -DartifactId=restapp -DinteractiveMode=false
```


* Una vez dentro del directorio raíz, crear el proyecto ejb:
	
```
#!bash

$ mvn archetype:generate -DarchetypeGroupId=org.codehaus.mojo.archetypes \
 -DarchetypeArtifactId=ejb-javaee7 \
 -DgroupId=com.example.restapp.ejb \
 -DartifactId=ejb \
 -DinteractiveMode=false
```


* Dentro del proyecto raíz crear el proyecto web:
	
```
#!bash

$ mvn archetype:generate -DarchetypeGroupId=org.codehaus.mojo.archetypes \
 -DarchetypeArtifactId=webapp-javaee7 \
 -DgroupId=com.example.restapp.web \
 -DartifactId=web \
 -DinteractiveMode=false
```


* Dentro del proyecto raíz crear el proyecto ear:
	
```
#!bash

$ mvn archetype:generate -DarchetypeGroupId=org.codehaus.mojo.archetypes \
 -DarchetypeArtifactId=ear-javaee7 \
 -DgroupId=com.example.restapp.ear \
 -DartifactId=ear \
 -DinteractiveMode=false
```



## Se crean los archivos:##
    * beans.xml
    * restapp-ds.xml
    * persistence.xml

Se procede a llenar los archivos pom.xml con las dependencias a ser utilizadas en los proyectos.